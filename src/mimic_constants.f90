!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2015-2022  The MiMiC Contributors (see CONTRIBUTORS file for details).
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

!> contains constants declarations MiMiC
module mimic_constants

use mimic_precision, only: dp

implicit none

    private

    public :: mimic_covrad
    public :: mimic_bohr

    !> covalent radii database in Angstrom
    real(dp), dimension(99), parameter :: mimic_covrad = [0.32_dp,  &
          0.93_dp,  1.23_dp,  0.90_dp,  0.82_dp,  0.77_dp,&
          0.75_dp,  0.73_dp,  0.72_dp,  0.71_dp,  1.54_dp,  &
          1.36_dp,  1.18_dp,  1.11_dp,  1.06_dp,  1.02_dp,  &
          0.99_dp,  0.98_dp,  2.03_dp,  1.74_dp,  1.44_dp,  &
          1.32_dp,  1.22_dp,  1.18_dp,  1.17_dp,  1.17_dp,  &
          1.16_dp,  1.15_dp,  1.17_dp,  1.25_dp,  1.26_dp,  &
          1.22_dp,  1.20_dp,  1.16_dp,  1.14_dp,  1.12_dp,&
          2.16_dp,  1.91_dp,  1.62_dp,  1.45_dp,  1.34_dp,  &
          1.30_dp,  1.27_dp,  1.25_dp,  1.25_dp,  1.28_dp,  &
          1.34_dp,  1.48_dp,  1.44_dp,  1.41_dp,  1.40_dp,  &
          1.36_dp,  1.33_dp,  1.31_dp,  2.35_dp,  1.98_dp,  &
          1.69_dp,  1.65_dp,  1.65_dp,  1.64_dp,  1.63_dp,  &
          1.62_dp,  1.85_dp,  1.61_dp,  1.59_dp,  1.59_dp,&
          1.58_dp,  1.57_dp,  1.56_dp,  1.56_dp,  1.56_dp,  &
          1.44_dp,  1.34_dp,  1.30_dp,  1.28_dp,  1.26_dp,  &
          1.27_dp,  1.30_dp,  1.34_dp,  1.49_dp,  1.48_dp,  &
          1.47_dp,  1.46_dp,  1.46_dp,  1.45_dp,  0.00_dp,  &
          0.00_dp,  0.00_dp,  0.00_dp,  1.65_dp,0.00_dp,  &
          1.42_dp,  0.00_dp,  0.00_dp,  0.00_dp,  0.00_dp,&
          0.00_dp,  0.00_dp,  0.00_dp]

    !> conversion factor from Angstrom to Bohr
    real(dp), parameter :: mimic_bohr = 1.0_dp/0.529177210859_dp

end module mimic_constants
