variables:
  GET_SOURCES_ATTEMPTS: "10"

stages:
  - build
  - test
  - deploy

pages:
  image: fedora:latest
  stage: deploy
  script:
    - dnf install -y -q cpp python3-pip
    - pip install ford
    - ford docs/MiMiC.md
    - mv docs/public/ public/
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

# Problems with Intel oneAPI both for pFUnit and MiMiC so for now we
# allow the job to fail
intel-oneapi:
  image: fedora:latest
  stage: build
  interruptible: true
  allow_failure: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . /opt/intel/oneapi/setvars.sh
    - . scripts/install_mcl.sh icx icpx ifx
    - . scripts/install_pfunit.sh icx icpx ifx
    - mkdir build && cd build
    - CC=icx CXX=icpx FC=ifx cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

intel-oneapi-debug:
  image: fedora:latest
  stage: build
  interruptible: true
  allow_failure: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . /opt/intel/oneapi/setvars.sh
    - . scripts/install_mcl.sh icx icpx ifx
    - . scripts/install_pfunit.sh icx icpx ifx
    - mkdir build && cd build
    - CC=icx CXX=icpx FC=ifx cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Debug ..
    - make
    - ctest --output-on-failure

intel-classic:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . /opt/intel/oneapi/setvars.sh
    - . scripts/install_mcl.sh icc icpc ifort
    - . scripts/install_pfunit.sh icc icpc ifort
    - mkdir build && cd build
    - CC=icc CXX=icpc FC=ifort cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

# Debug job fails to build with pFUnit
intel-classic-debug:
  image: fedora:latest
  stage: build
  interruptible: true
  allow_failure: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . /opt/intel/oneapi/setvars.sh
    - . scripts/install_mcl.sh icc icpc ifort
    - . scripts/install_pfunit.sh icc icpc ifort
    - mkdir build && cd build
    - CC=icc CXX=icpc FC=ifort cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Debug ..
    - make
    - ctest --output-on-failure

intel-intelmpi:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . /opt/intel/oneapi/setvars.sh
    - . scripts/install_mcl.sh mpiicc mpiicpc mpiifort
    - . scripts/install_pfunit.sh mpiicc mpiicpc mpiifort
    - mkdir build && cd build
    - CC=mpiicc CXX=mpiicpc FC=mpiifort cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

intel-openmpi:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . ~/.bash_profile
    - . /opt/intel/oneapi/setvars.sh
    - module load mpi/openmpi
    - . scripts/install_mcl.sh mpicc mpicxx mpifort
    - . scripts/install_pfunit.sh mpicc mpicxx mpifort
    - mkdir build && cd build
    - CC=mpicc CXX=mpicxx FC=mpifort cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DMPIEXEC_PREFLAGS="--allow-run-as-root;--oversubscribe" -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

intel-mpich:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
    - . scripts/setup_intel_repo.sh
    - . scripts/install_intel.sh
  script:
    - . ~/.bash_profile
    - . /opt/intel/oneapi/setvars.sh
    - module load mpi/mpich
    - . scripts/install_mcl.sh mpicc mpicxx mpifort
    - . scripts/install_pfunit.sh mpicc mpicxx mpifort
    - mkdir build && cd build
    - CC=mpicc CXX=mpicxx FC=mpifort cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

gcc-openmpi:
  image: fedora:latest
  stage: build
  interruptible: true
  coverage: '/Percentage Coverage: \d+\.\d+/'
  before_script:
    - . scripts/install_prerequisites.sh
  script:
    - . ~/.bash_profile
    - module load mpi/openmpi
    - . scripts/install_mcl.sh gcc g++ gfortran
    - . scripts/install_pfunit.sh gcc g++ gfortran
    - mkdir build && cd build
    - CC=gcc CXX=g++ FC=gfortran cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DENABLE_COVERAGE=YES -DMPIEXEC_PREFLAGS="--allow-run-as-root;--oversubscribe" -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure
    - ctest -D ExperimentalCoverage

gcc-mpich:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
  script:
    - . ~/.bash_profile
    - module load mpi/mpich
    - . scripts/install_mcl.sh gcc g++ gfortran
    - . scripts/install_pfunit.sh gcc g++ gfortran
    - mkdir build && cd build
    - CC=gcc CXX=g++ FC=gfortran cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DCMAKE_BUILD_TYPE=Release ..
    - make
    - ctest --output-on-failure

gcc-debug:
  image: fedora:latest
  stage: build
  interruptible: true
  before_script:
    - . scripts/install_prerequisites.sh
  script:
    - . ~/.bash_profile
    - module load mpi/openmpi
    - . scripts/install_mcl.sh gcc g++ gfortran
    - . scripts/install_pfunit.sh gcc g++ gfortran
    - mkdir build && cd build
    - CC=gcc CXX=g++ FC=gfortran cmake -DENABLE_OPENMP=ON -DBUILD_TESTS=YES -DCMAKE_PREFIX_PATH=pFUnit/build/installed/PFUNIT-4.5/cmake -DENABLE_COVERAGE=YES -DMPIEXEC_PREFLAGS="--allow-run-as-root;--oversubscribe" -DCMAKE_BUILD_TYPE=Debug ..
    - make
    - ctest --output-on-failure

