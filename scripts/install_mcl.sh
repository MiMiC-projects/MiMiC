#!/bin/bash

SAVE_DIR=$PWD
git clone --branch=2.0.1 --single-branch --depth=1 https://gitlab.com/MiMiC-projects/CommLib.git
mkdir CommLib/build && cd CommLib/build
CC=${1} CXX=${2} FC=${3} cmake ..
make install
cd $SAVE_DIR
