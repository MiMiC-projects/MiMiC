#!/bin/bash

SAVE_DIR=$PWD
git clone --branch=v4.5.0 --single-branch --depth=1 https://github.com/Goddard-Fortran-Ecosystem/pFUnit.git
cp pFUnit/cmake/Intel.cmake pFUnit/cmake/IntelLLVM.cmake
mkdir pFUnit/build && cd pFUnit/build
CC=${1} CXX=${2} FC=${3} cmake ..
make install
cd $SAVE_DIR
